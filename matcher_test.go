package matcher

import (
	"fmt"
	"testing"
	"time"

	xslices "gitee.com/jiaoyuedave/go-utils/slices"
)

type TestCandidate struct {
	Id      int
	removed bool
}

type TestPool []*TestCandidate

func newTestPool(size int) TestPool {
	pool := make([]*TestCandidate, 0, size)
	for i := 0; i < size; i++ {
		pool = append(pool, &TestCandidate{Id: i})
	}
	return pool
}

const (
	testFiltTagDefault FiltTag = iota
	testFiltTagRemoved
	testFiltTagMatched
)

type TestInstantAdapter []*TestCandidate

func (a TestInstantAdapter) Len() int {
	return len(a)
}

func (a TestInstantAdapter) Match(i, j int) bool {
	if i == j {
		panic(fmt.Sprintf("i == j: %d == %d", i, j))
	}
	if i >= len(a) || j >= len(a) {
		panic(fmt.Sprintf("index out of range: %d, %d", i, j))
	}
	return true
}

func (a TestInstantAdapter) Filt(i int) FiltTag {
	if a[i].removed {
		return testFiltTagRemoved
	}
	return testFiltTagDefault
}

type TestTimeoutAdapter []*TestCandidate

func (a TestTimeoutAdapter) Len() int {
	return len(a)
}

func (a TestTimeoutAdapter) Match(i, j int) bool {
	time.Sleep(1 * time.Millisecond)
	return false
}

func (a TestTimeoutAdapter) Filt(i int) FiltTag {
	return testFiltTagDefault
}

type TestTeam []struct{}

type TestTeamPool []TestTeam

func newTestTeamPool(size int) TestTeamPool {
	pool := make(TestTeamPool, 0, size)
	for i := 0; i < size; i++ {
		pool = append(pool, TestTeam{struct{}{}})
	}
	return pool
}

type TestReduceAdapter struct {
	TeamNum int
	teams   TestTeamPool
}

func (a *TestReduceAdapter) Len() int {
	return len(a.teams)
}

func (a *TestReduceAdapter) Match(i, j int) bool {
	return len(a.teams[i])+len(a.teams[j]) <= a.TeamNum
}

func (a *TestReduceAdapter) Reduce(i, j int) {
	a.teams[i] = append(a.teams[i], a.teams[j]...)
}

func (a *TestReduceAdapter) Filt(i int) FiltTag {
	if len(a.teams[i]) == a.TeamNum {
		return testFiltTagMatched
	}
	return testFiltTagDefault
}

func (a *TestReduceAdapter) DataSlice() interface{} {
	return a.teams
}

func (a *TestReduceAdapter) SetDataSlice(s interface{}) {
	a.teams = s.(TestTeamPool)
}

func TestTimeoutMatcher(t *testing.T) {
	matcher := NewTimeoutFiltMatcher(func(tmo *TimeoutFiltMatcherOptions) {
		tmo.Timeout = 1 * time.Second
		tmo.MaxMatch = 100
		tmo.MaxCheck = 0
		tmo.MinCheck = 10
	})
	cases := []struct {
		name string
		size int
	}{
		{
			name: "Small Pool",
			size: 50,
		},
		{
			name: "Large Pool",
			size: 5000,
		},
		{
			name: "One Match",
			size: 2,
		},
		{
			name: "Empty",
			size: 0,
		},
	}
	for _, c := range cases {
		t.Run(c.name, func(t *testing.T) {
			pool := newTestPool(c.size)
			var res [][2]int

			for {
				r, filtMap, _ := matcher.FiltFindMatches(TestInstantAdapter(pool))
				for _, pair := range r {
					if pool[pair[0]].removed || pool[pair[1]].removed {
						t.Errorf("pair: %v, removed: %v, %v", pair, pool[pair[0]].removed, pool[pair[1]].removed)
					}
					pool[pair[0]].removed = true
					pool[pair[1]].removed = true
					res = append(res, pair)
				}
				remove := filtMap[testFiltTagRemoved]
				pool = xslices.Clear(pool, remove).(TestPool)
				if len(pool) == 0 {
					break
				}
			}
			if len(res) != c.size/2 {
				t.Errorf("matchedCnt: %d, want: %d", len(res), c.size/2)
			}
		})
	}
}

func TestTimeoutMatcher_MaxMatch(t *testing.T) {
	pool := newTestPool(10000)
	matcher := NewTimeoutFiltMatcher(func(tmo *TimeoutFiltMatcherOptions) {
		tmo.Timeout = 1 * time.Second
		tmo.MaxMatch = 100
		tmo.MaxCheck = 0
		tmo.MinCheck = 10
	})
	res, _, err := matcher.FiltFindMatches(TestInstantAdapter(pool))
	if err != nil {
		t.Errorf("err: %v", err)
	}
	if len(res) != 100 {
		t.Errorf("matchedCnt: %d, want: %d", len(res), 100)
	}
}

func TestTimeoutMatcher_Timeout(t *testing.T) {
	pool := newTestPool(10000)
	matcher := NewTimeoutFiltMatcher(func(tmo *TimeoutFiltMatcherOptions) {
		tmo.Timeout = 100 * time.Millisecond
		tmo.MinCheck = 10
	})
	_, _, err := matcher.FiltFindMatches(TestTimeoutAdapter(pool))
	if err != ErrTimeout {
		t.Errorf("err: %v, want: %v", err, ErrTimeout)
	}
}

func TestTimeoutMatcher_MaxCheck(t *testing.T) {
	pool := newTestPool(10000)
	matcher := NewTimeoutFiltMatcher(func(tmo *TimeoutFiltMatcherOptions) {
		tmo.Timeout = 1 * time.Second
		tmo.MaxCheck = 100
		tmo.MinCheck = 10
	})
	_, _, err := matcher.FiltFindMatches(TestTimeoutAdapter(pool))
	if err != ErrMaxCheck {
		t.Errorf("err: %v, want: %v", err, ErrMaxCheck)
	}
}

func TestTimeoutFiltMatcher(t *testing.T) {
	matcher := NewTimeoutFiltMatcher(func(tmo *TimeoutFiltMatcherOptions) {
		tmo.Timeout = 1 * time.Second
		tmo.MaxMatch = 100
		tmo.MaxCheck = 0
		tmo.MinCheck = 10
	})
	cases := []struct {
		name string
		size int
	}{
		{
			name: "Small Pool",
			size: 50,
		},
		{
			name: "Large Pool",
			size: 5000,
		},
		{
			name: "One Match",
			size: 2,
		},
		{
			name: "Empty",
			size: 0,
		},
	}
	for _, c := range cases {
		t.Run(c.name, func(t *testing.T) {
			pool := newTestTeamPool(c.size)
			var res []TestTeam
			for {
				r, filtMap, _ := matcher.FiltFindMatches(&TestReduceAdapter{2, pool})
				remove := make([]int, 0, len(r))
				for _, pair := range r {
					pool[pair[0]] = append(pool[pair[0]], pool[pair[1]]...)
					remove = append(remove, pair[1])
				}
				for _, idx := range filtMap[testFiltTagMatched] {
					res = append(res, pool[idx])
					remove = append(remove, idx)
				}
				pool = xslices.Clear(pool, remove).(TestTeamPool)
				if len(pool) == 0 {
					break
				}
			}
			if len(res) != c.size/2 {
				t.Errorf("matchedCnt: %d, want: %d", len(res), c.size/2)
			}
		})
	}
}

func TestTimeoutReduceMatcher(t *testing.T) {
	// 注意，n取较大的数的时候case会失败，是因为有可能会组成少量的三人队
	// n := 1000
	n := 20
	pool := newTestTeamPool(n)
	var a ReduceMatchAdapter = &TestReduceAdapter{4, pool}
	matcher := NewTimeoutReduceMatcher(func(tmo *TimeoutReduceMatcherOptions) {
		tmo.ReduceTimeout = 100 * time.Millisecond
		tmo.MaxReduce = 10
		tmo.MaxFilt = 10000
	})
	var res []TestTeam
	for {
		a, _ = matcher.ReduceFindMatches(a)
		var f interface{}
		a, f, _ = matcher.FiltMatches(a)
		for _, team := range f.(TestTeamPool) {
			res = append(res, team)
		}
		if a.Len() == 0 {
			break
		}
	}
	if len(res) != n/4 {
		t.Errorf("matchedCnt: %d, want: %d", len(res), n/4)
	}
}
