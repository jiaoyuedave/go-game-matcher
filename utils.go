package matcher

import xslices "gitee.com/jiaoyuedave/go-utils/slices"

func ReducePairs(data ReduceAdapter, pairs [][2]int) ReduceAdapter {
	remove := make([]int, 0, len(pairs))
	for _, p := range pairs {
		data.Reduce(p[0], p[1])
		remove = append(remove, p[1])
	}
	d := xslices.Clear(data.DataSlice(), remove)
	data.SetDataSlice(d)
	return data
}
