package matcher

import "fmt"

func newErr(msg string) error {
	return fmt.Errorf("matcher: %s", msg)
}
