package matcher

import (
	"math/rand"
	"time"

	xslices "gitee.com/jiaoyuedave/go-utils/slices"
)

type FiltTag int

const FiltTagDefault FiltTag = 0 // this tag should not be included in the result

type FiltMatchAdapter interface {
	Len() int
	Match(i, j int) bool
	Filt(i int) FiltTag
}

type ReduceAdapter interface {
	Reduce(i, j int)
	DataSlice() interface{}
	SetDataSlice(d interface{})
}

type ReduceMatchAdapter interface {
	FiltMatchAdapter
	ReduceAdapter
}

type FiltMatcher interface {
	FiltFindMatches(data FiltMatchAdapter) (res [][2]int, filterMap map[FiltTag][]int, err error)
}

type ReduceMatcher interface {
	ReduceFindMatches(data ReduceMatchAdapter) (ReduceMatchAdapter, error)
	FiltMatches(data ReduceMatchAdapter) (remained ReduceMatchAdapter, filtered interface{}, err error)
}

var (
	ErrTimeout   = newErr("timeout")
	ErrMaxCheck  = newErr("max check")
	ErrExhausted = newErr("exhausted")
)

type TimeoutFiltMatcherOptions struct {
	Timeout  time.Duration
	MaxMatch int
	MaxCheck int
	MinCheck int
}

func NewDefaultTimeoutFiltMatcherOptions() *TimeoutFiltMatcherOptions {
	return &TimeoutFiltMatcherOptions{
		Timeout:  100 * time.Millisecond,
		MaxMatch: 100,
		MaxCheck: 10000,
		MinCheck: 100,
	}
}

type TimeoutFiltMatcherOption func(*TimeoutFiltMatcherOptions)

type TimeoutFiltMatcher struct {
	options *TimeoutFiltMatcherOptions
}

func NewTimeoutFiltMatcher(options ...TimeoutFiltMatcherOption) *TimeoutFiltMatcher {
	m := &TimeoutFiltMatcher{
		options: NewDefaultTimeoutFiltMatcherOptions(),
	}
	for _, option := range options {
		option(m.options)
	}
	return m
}

func (m *TimeoutFiltMatcher) FiltFindMatches(data FiltMatchAdapter) (res [][2]int, filterMap map[FiltTag][]int, err error) {
	filterMap = make(map[FiltTag][]int)
	if data.Len() < 2 {
		for i := 0; i < data.Len(); i++ {
			if tag := data.Filt(i); tag != FiltTagDefault {
				filterMap[tag] = append(filterMap[tag], i)
			}
		}
		return res, filterMap, ErrExhausted
	}
	i := rand.Intn(data.Len())
	j := rand.Intn(data.Len() - 1)
	if j >= i {
		j++
	} else {
		i, j = j, i
	}
	p, q := i, j
	check, totalCheck, match := 0, 0, 0
	matched := make(map[int]struct{})
	filtered := make(map[int]bool)
	start := time.Now()
	for {
		if _, ok := matched[p]; ok {
			goto check_and_next_p
		}
		if f, ok := filtered[p]; ok {
			if f {
				goto check_and_next_p
			}
		} else {
			if tag := data.Filt(p); tag != FiltTagDefault {
				filterMap[tag] = append(filterMap[tag], p)
				filtered[p] = true
				goto next_p
			} else {
				filtered[p] = false
			}
		}

		for q < data.Len() {
			if _, ok := matched[q]; ok {
				goto next_q
			}
			if f, ok := filtered[q]; ok {
				if f {
					goto next_q
				}
			} else {
				if tag := data.Filt(q); tag != FiltTagDefault {
					filterMap[tag] = append(filterMap[tag], q)
					filtered[q] = true
					goto next_q
				} else {
					filtered[q] = false
				}
			}
			if m.options.MaxCheck > 0 && totalCheck >= m.options.MaxCheck {
				return res, filterMap, ErrMaxCheck
			}
			if check >= m.options.MinCheck && m.options.Timeout > 0 {
				check = 0
				if time.Since(start) >= m.options.Timeout {
					return res, filterMap, ErrTimeout
				}
			}
			if data.Match(p, q) {
				res = append(res, [2]int{p, q})
				matched[p] = struct{}{}
				matched[q] = struct{}{}
				match++
				if m.options.MaxMatch > 0 && match >= m.options.MaxMatch {
					return res, filterMap, nil
				}
				break
			}
			check++
			totalCheck++

		next_q:
			q++
			if p == i && q == j {
				return res, filterMap, ErrExhausted
			}
		}
		goto next_p

	check_and_next_p:
		if p == i {
			return res, filterMap, ErrExhausted
		}

	next_p:
		p++
		if p == data.Len() {
			p = 0
		}
		q = p + 1
		if p == i && q == j {
			return res, filterMap, ErrExhausted
		}
	}
}

var (
	ErrMaxReduce = newErr("max reduce")
	ErrMaxFilt   = newErr("max filt")
)

type TimeoutReduceMatcherOptions struct {
	ReduceTimeout time.Duration
	FiltTimeout   time.Duration
	MaxReduce     int
	MaxFilt       int
}

func NewDefaultTimeoutReduceMatcherOptions() *TimeoutReduceMatcherOptions {
	return &TimeoutReduceMatcherOptions{
		ReduceTimeout: 100 * time.Millisecond,
		FiltTimeout:   100 * time.Millisecond,
		MaxReduce:     10,
		MaxFilt:       10000,
	}
}

type TimeoutReduceMatcherOption func(*TimeoutReduceMatcherOptions)

type TimeoutReduceMatcher struct {
	matcher FiltMatcher
	options *TimeoutReduceMatcherOptions
}

func NewTimeoutReduceMatcher(options ...TimeoutReduceMatcherOption) *TimeoutReduceMatcher {
	opt := NewDefaultTimeoutReduceMatcherOptions()
	for _, option := range options {
		option(opt)
	}
	m := &TimeoutReduceMatcher{
		matcher: NewTimeoutFiltMatcher(func(tmo *TimeoutFiltMatcherOptions) {
			tmo.Timeout = opt.ReduceTimeout
			tmo.MaxMatch = opt.MaxReduce
			tmo.MaxCheck = 0
			tmo.MinCheck = 10
		}),
		options: opt,
	}
	return m
}

func (m *TimeoutReduceMatcher) ReduceFindMatches(data ReduceMatchAdapter) (ReduceMatchAdapter, error) {
	start := time.Now()
	var reduce int
	var err error
	for {
		if time.Since(start) >= m.options.ReduceTimeout {
			err = ErrTimeout
			break
		}
		var res [][2]int
		res, _, err = m.matcher.FiltFindMatches(data)
		if err == ErrExhausted && len(res) == 0 {
			err = ErrExhausted
			break
		}
		remove := make([]int, 0, len(res))
		for _, v := range res {
			data.Reduce(v[0], v[1])
			remove = append(remove, v[1])
		}
		d := xslices.Clear(data.DataSlice(), remove)
		data.SetDataSlice(d)
		reduce += len(res)
		if m.options.MaxReduce > 0 && reduce >= m.options.MaxReduce {
			err = ErrMaxReduce
			break
		}
	}
	return data, err
}

func (m *TimeoutReduceMatcher) FiltMatches(data ReduceMatchAdapter) (remained ReduceMatchAdapter, filtered interface{}, err error) {
	start := time.Now()
	i := rand.Intn(data.Len())
	j := i
	var filtIndexes []int
	for {
		if time.Since(start) >= m.options.FiltTimeout {
			err = ErrTimeout
			break
		}
		if data.Filt(i) != FiltTagDefault {
			filtIndexes = append(filtIndexes, i)
		}
		if m.options.MaxFilt > 0 && len(filtIndexes) >= m.options.MaxFilt {
			err = ErrMaxFilt
			break
		}
		i++
		if i == data.Len() {
			i = 0
		}
		if i == j {
			err = ErrExhausted
			break
		}
	}
	r, p := xslices.Pop(data.DataSlice(), filtIndexes)
	data.SetDataSlice(r)
	return data, p, err
}
